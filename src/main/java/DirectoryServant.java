// DirectoryServant.java
import eds.InvalidLogin;
import eds.department;
import eds.user;

import java.util.*;

public class DirectoryServant extends eds._directoryImplBase
{

  // Use lists to store application state
  List<user> users = new ArrayList<user>();
  List<user> managers = new ArrayList<user>();
  List<department> departments = new ArrayList<department>();

  // Methods required to implement directory interface
  public eds.user login_user (String username, String password) throws eds.InvalidLogin
  {
    if(users.isEmpty() && managers.isEmpty())
    {
        throw new InvalidLogin("No users loaded on server for login");
    }
    for(user u : users)
    {
        if (u.username().equals(username))
            if (u.password().equals(password)) {
                return u;
            }
    }
    for(user m : managers) {
        if (m.username().equals(username))
            if (m.password().equals(password)) {
                return m;
            }
    }
    throw new InvalidLogin("Login was not successful for user " + username);
  }
  
}
