package Server;
/*
 * Shane Ryan 10340427
 *
 * Created on 21/09/13
 *
 */

import au.com.bytecode.opencsv.CSVReader;
import au.com.bytecode.opencsv.bean.ColumnPositionMappingStrategy;
import au.com.bytecode.opencsv.bean.CsvToBean;

import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.IOException;
import java.io.Reader;
import java.util.ArrayList;
import java.util.List;

public class UserReader {

    private CSVReader reader =  null;
    private List<User> users = new ArrayList<User>();

    public UserReader(String filename) throws FileNotFoundException {
        Reader ioReader = new FileReader(filename);
        reader = new CSVReader(ioReader);
    }

    public final void readAll() throws IOException {
        ColumnPositionMappingStrategy<User> strategy = new ColumnPositionMappingStrategy<>();
        strategy.setType(User.class);
        String[] columns = new String[] {"id", "username", "password", "name", "deptName", "email", "tel", "location"};
        strategy.setColumnMapping(columns);

        CsvToBean csv = new CsvToBean();
        users = csv.parse(strategy, reader);
    }

    public final User getUserById(final int id) {
        for(User u : users) {
            if(u.getId() == id) {
                return u;
            }
        }
        return null;
    }

    public final User getUserByName(final String name) {
        for(User u : users) {
            if(u.getName().equals(name)) {
                return u;
            }
        }
        return null;
    }

    public List<User> getAllUsers() {
        return users;
    }
}
