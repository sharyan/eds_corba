package Server;
/*
 * Shane Ryan 10340427
 *
 * Created on 21/09/13
 *
 */

/*
TODO
Name
CORBA based Employee Directory Service
Instructions
The goal of this assignment is to implement and test a simple Employee Directory Service using CORBA technology.
The following design guidelines will apply:
It should allow any user to view all other entries.
The service should allow users to modify their own entries
The list of interfaces include directory, user and department
The server should load its data on startup from one or more CSV files
To keep the task simpler, nesting of departments (orgchart type functionality) is not required!
The required IDL file is supplied as well as the basic definitions for the required server implementation classes.
You will need to compile the supplied IDL file and complete the code for the implementation classes. You should then create a server application that instantiates a single instance of the directory implementation class and binds this into the CORBA Names Server. You will also need to create a simple client application that interacts with the server as required to fully test the server functionality. A GUI based client application is not required.
The assignment should be done in groups of two students. It may be convenient for one student to work on the client code while the other implments the server side functionality. Where the assignment is submitted by one student then don't forget to mention the name and ID number of the other person in your group so that they will also be credited for the assignment.
When completed you should submit copies of the code you have written for the assignment as well as a description of how you tested it and screen shots of the application running. All submissions should be done via Blackboard and if you submit more than one attempt then only the final attempt will be marked.

 */

import java.io.FileNotFoundException;
import java.io.IOException;
import java.util.List;

public class ServerMain {

    public static void main(final String[] args)
    {
        getDepartmentData();
        getUserData();

        // Register server with name server




        Object sync = new Object();

        try {
            sync.wait();
        } catch (InterruptedException e) {
            System.err.println("Server is shutting down");
        }
    }

    private static void getDepartmentData() {
        DepartmentReader deptReader = null;
        try {
            deptReader = new DepartmentReader("src\\main\\resources\\department.csv");
        } catch (FileNotFoundException e) {
            System.err.println("Could not find department data");
        }
        if (deptReader != null) {
            try {
                deptReader.readAll();
            } catch (IOException e) {
                System.err.println("Error reading department data from CSV file");
                e.printStackTrace();
            }
            List<Department> departments = deptReader.getAllDepartments();
            for (Department d : departments) {
                System.out.println(d.toString());
            }
        }
    }

    private static void getUserData() {
        UserReader userReader = null;
        try {
            userReader = new UserReader("src\\main\\resources\\user.csv");
        } catch (FileNotFoundException e) {
            System.err.println("Could not find user data");
        }
        if (userReader != null) {
            try {
                userReader.readAll();
            } catch (IOException e) {
                System.err.println("Error reading user data from CSV file");
                e.printStackTrace();
            }
            List<User> users = userReader.getAllUsers();
            for (User u : users) {
                System.out.println(u.toString());
            }
        }
    }
}
