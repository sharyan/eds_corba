package Server;
/*
 * Shane Ryan 10340427
 *
 * Created on 22/09/13
 *
 */

public class User {

    //id,username,password,name,deptName,email,tel,location
    private int id;
    private String username;
    private String password;
    private String name;
    private String deptName;
    private String email;
    private String tel;
    private String location;

    public User() {
    }

    public User(int Id, String Username, String Password, String Name,
                String DeptName, String Email, String Tel, String Location) {
        id = Id;
        username = Username;
        password = Password;
        name = Name;
        deptName = DeptName;
        tel = Tel;
        location = Location;
    }


    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getUsername() {
        return username;
    }

    public void setUsername(String username) {
        this.username = username;
    }

    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getDeptName() {
        return deptName;
    }

    public void setDeptName(String deptName) {
        this.deptName = deptName;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public String getTel() {
        return tel;
    }

    public void setTel(String tel) {
        this.tel = tel;
    }

    public String getLocation() {
        return location;
    }

    public void setLocation(String location) {
        this.location = location;
    }

    @Override
    public String toString() {
        return "User{" +
                "id=" + id +
                ", username='" + username + '\'' +
                ", password='" + password + '\'' +
                ", name='" + name + '\'' +
                ", deptName='" + deptName + '\'' +
                ", email='" + email + '\'' +
                ", tel='" + tel + '\'' +
                ", location='" + location + '\'' +
                '}';
    }
}
