package Server;
/*
 * Shane Ryan 10340427
 *
 * Created on 21/09/13
 *
 */



import au.com.bytecode.opencsv.CSVReader;
import au.com.bytecode.opencsv.bean.ColumnPositionMappingStrategy;
import au.com.bytecode.opencsv.bean.CsvToBean;

import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.IOException;
import java.io.Reader;
import java.util.ArrayList;
import java.util.List;

public class DepartmentReader {

    private CSVReader reader =  null;
    private List<Department> departments = new ArrayList<Department>();

    public DepartmentReader(String filename) throws FileNotFoundException {
        Reader ioReader = new FileReader(filename);
        reader = new CSVReader(ioReader);
    }

    public void readAll() throws IOException {
        ColumnPositionMappingStrategy<Department> strategy = new ColumnPositionMappingStrategy<>();
        strategy.setType(Department.class);
        String[] columns = new String[] {"id", "name", "location", "managerName"};
        strategy.setColumnMapping(columns);

        CsvToBean csv = new CsvToBean();
        departments = csv.parse(strategy, reader);
    }

    public Department getDepartmentById(final int id) {
        for(Department d : departments) {
            if(d.getId() == id) {
                return d;
            }
        }
        return null;
    }

    public Department getDepartmentByName(final String name) {
        for(Department d : departments) {
            if(d.getName().equals(name)) {
                return d;
            }
        }
        return null;
    }

    public List<Department> getAllDepartments() {
        return departments;
    }

}
