package Server;
/*
 * Shane Ryan 10340427
 *
 * Created on 21/09/13
 *
 */

import eds.deptdetails;

public class Department {

    private int id;
    private String name;
    private String location;
    private String managerName;

    public Department() {
        this.id = -1;
        this.name = "";
        this.location = "";
        this.managerName = "";
    }

    public Department(final int id, final String name, final String location, final String managerName) {
        this.id = id;
        this.name = name;
        this.location = location;
        this.managerName = managerName;
    }


    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getLocation() {
        return location;
    }

    public void setLocation(String location) {
        this.location = location;
    }

    public String getManagerName() {
        return managerName;
    }

    public void setManagerName(String managerName) {
        this.managerName = managerName;
    }

    public deptdetails getDepartmentDetails() {
        deptdetails details = new deptdetails();
    }

    @Override
    public String toString() {
        return "Department{" +
                "id=" + id +
                ", name='" + name + '\'' +
                ", location='" + location + '\'' +
                ", managerName='" + managerName + '\'' +
                '}';
    }
}
