// UserServant.java

import eds.InvalidArgument;
import eds.department;

import java.util.*;

public class UserServant extends eds._userImplBase
{
  private eds.userdetails mydetails;
  private String mydeptname;
  private int myid;
  private List departments;

  // Constructor
  public UserServant(eds.userdetails mydetails, String mydeptname, int myid, List departments)
  {
    this.mydetails = mydetails;
    this.mydeptname = mydeptname;
    this.myid = myid;
    this.departments = departments;
  }

  // Methods required to implement user interface
  public eds.userdetails details ()
  {
    return mydetails;
  }

  public void details (eds.userdetails newDetails)
  {
    mydetails = newDetails;
  }

  public String deptName ()
  {
    return  mydeptname;
  }

    public String username ()
{
    return "";
}

    public String password() {
        return "";
    }

  public int id ()
  {
    return myid;
  }

  public eds.department[] getAllDepartments ()
  {
      return (eds.department[])departments.toArray();
  }

  public eds.department getDepartment (String name) throws eds.InvalidArgument
  {
      department[] departs = (eds.department[])departments.toArray();
      for(department d : departs) {
          if(d.details().name.equalsIgnoreCase(name)) {
              return d;
          }
      }
      throw new InvalidArgument("Department of name " + name + " does not exist in driectory");
  }

  public void setPassword (String oldpassword, String newpassword) throws eds.InvalidArgument
  {

  }

}
