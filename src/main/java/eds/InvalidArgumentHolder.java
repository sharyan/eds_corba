package eds;

/**
* eds/InvalidArgumentHolder.java .
* Generated by the IDL-to-Java compiler (portable), version "3.2"
* from eds.idl
* 21 September 2013 16:16:51 o'clock IST
*/

public final class InvalidArgumentHolder implements org.omg.CORBA.portable.Streamable
{
  public eds.InvalidArgument value = null;

  public InvalidArgumentHolder ()
  {
  }

  public InvalidArgumentHolder (eds.InvalidArgument initialValue)
  {
    value = initialValue;
  }

  public void _read (org.omg.CORBA.portable.InputStream i)
  {
    value = eds.InvalidArgumentHelper.read (i);
  }

  public void _write (org.omg.CORBA.portable.OutputStream o)
  {
    eds.InvalidArgumentHelper.write (o, value);
  }

  public org.omg.CORBA.TypeCode _type ()
  {
    return eds.InvalidArgumentHelper.type ();
  }

}
