// DepartmentServant.java
import Server.Department;
import Server.DepartmentReader;
import eds.InvalidArgument;
import eds.user;
import eds.userdetails;

import java.io.FileNotFoundException;
import java.io.IOException;
import java.util.*;

public class DepartmentServant extends eds._departmentImplBase
{

    private eds.deptdetails mydetails;
    private List<user> users;

    // Constructor
    public DepartmentServant(eds.deptdetails mydetails, List users)
    {
        this.mydetails = mydetails;
        this.users = (List<user>)users;
    }

    // Methods required to implement department interface
    public eds.deptdetails details ()
    {

    }

    public eds.userdetails[] getAllUserDetails ()
    {

    }

    public eds.userdetails getUserDetails (int id) throws eds.InvalidArgument
    {

    }
}
